package com.grainchain.grainchaintest.fragments.userlist

import com.grainchain.grainchaintest.database.Contacts
import com.grainchain.grainchaintest.fragments.userlist.adapters.UsersListAdapter
import com.grainchain.grainchaintest.models.ModelSqlite
import com.grainchain.grainchaintest.models.ModelsContract

class UsersListPresenter(val view: UsersListContract.View) : UsersListContract.Presenter {

    private val modelSqlite: ModelsContract.ModelSqlite = ModelSqlite()
    private val list: ArrayList<Contacts> = arrayListOf()
    private lateinit var adapter: UsersListAdapter

    override fun initView() {
        initRecyler()
    }

    private fun initRecyler() {
        adapter = UsersListAdapter(list, ::deleteItem)
        list.addAll(modelSqlite.getContacts())
        checkEmptyList()
        view.initRecycler(adapter)
    }

    private fun checkEmptyList() {
        if (list.isEmpty()) {
            view.showEmptyText()
        } else {
            view.hideEmptyText()
        }
    }

    private fun deleteItem(position: Int) {
        view.showDeleteConfirmation {
            val obj = list[position]
            modelSqlite.deleteContact(obj.id!!)
            list.removeAt(position)
            checkEmptyList()
            adapter.notifyItemRemoved(position)
        }
    }

    override fun addContact(contact: Contacts) {
        list.add(contact)
        adapter.notifyItemInserted(list.size.minus(1))
    }

    override fun searchElement(text: String) {
        list.clear()
        list.addAll(modelSqlite.getContacts(text))
        checkEmptyList()
        adapter.notifyDataSetChanged()
    }
}