package com.grainchain.grainchaintest.fragments.userlist


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.MenuItemCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.*
import com.grainchain.grainchaintest.R
import com.grainchain.grainchaintest.createAlert
import com.grainchain.grainchaintest.database.Contacts
import com.grainchain.grainchaintest.fragments.userlist.adapters.UsersListAdapter
import kotlinx.android.synthetic.main.fragment_users_list.*

class UsersListFragment : Fragment(), UsersListContract.View {

    companion object {
        fun getNewInstance(): UsersListFragment = UsersListFragment()
    }

    private lateinit var presenter: UsersListContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_users_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = UsersListPresenter(this)
        presenter.initView()
    }

    override fun initRecycler(adapter: UsersListAdapter) {
        user_list_recycler_view.layoutManager = LinearLayoutManager(activity)
        user_list_recycler_view.adapter = adapter
    }

    override fun showEmptyText() {
        user_list_empty.visibility = View.VISIBLE
    }

    override fun hideEmptyText() {
        user_list_empty.visibility = View.GONE
    }

    override fun showDeleteConfirmation(eventDelete: () -> Unit) {
        activity!!.createAlert {
            setMessage(R.string.users_list_confirme_delete)
            setPositiveButton(R.string.ok_text) { dialog, _ ->
                eventDelete()
                dialog.dismiss()
            }
            setNegativeButton(R.string.cancel_text) { dialog, _ ->
                dialog.dismiss()
            }
        }
    }

    fun addContact(contact: Contacts) {
        presenter.addContact(contact)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)

        inflater?.inflate(R.menu.menu_filter, menu)

        val item = MenuItemCompat.getActionView(menu?.findItem(R.id.menu_filter_find_element)) as SearchView
        item.queryHint = getString(R.string.menu_filter_search_contact)
        item.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean = false

            override fun onQueryTextChange(newText: String?): Boolean {
                presenter.searchElement(newText!!)
                return true
            }
        })
    }

}
