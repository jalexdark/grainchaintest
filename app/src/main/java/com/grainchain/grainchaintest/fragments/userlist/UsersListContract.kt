package com.grainchain.grainchaintest.fragments.userlist

import com.grainchain.grainchaintest.fragments.userlist.adapters.UsersListAdapter
import com.grainchain.grainchaintest.database.Contacts

interface UsersListContract {
    interface View {
        fun initRecycler(adapter: UsersListAdapter)
        fun showEmptyText()
        fun hideEmptyText()
        fun showDeleteConfirmation(eventDelete: () -> Unit)
    }

    interface Presenter {
        fun initView()
        fun addContact(contact: Contacts)
        fun searchElement(text: String)
    }
}