package com.grainchain.grainchaintest.fragments.useradd

import com.grainchain.grainchaintest.database.Contacts

interface UsersAddContract {

    interface View {
        fun notifyList(contact: Contacts)
        fun showAlert(msg: String)
        fun clearFields()
    }

    interface Presenter {
        fun clickSubmit(name: String, lastname: String, age: String, phone: String)
    }

    interface OnContactCreated {
        fun onContactCreated(contact: Contacts)
    }
}