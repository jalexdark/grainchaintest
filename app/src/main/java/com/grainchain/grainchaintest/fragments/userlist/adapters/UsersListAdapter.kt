package com.grainchain.grainchaintest.fragments.userlist.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.grainchain.grainchaintest.GrainChain
import com.grainchain.grainchaintest.R
import com.grainchain.grainchaintest.database.Contacts
import kotlinx.android.synthetic.main.item_users_list.view.*

class UsersListAdapter(val list: List<Contacts>, val eventDelete: (Int) -> Unit) : RecyclerView.Adapter<UsersListAdapter.UsersListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): UsersListViewHolder =
            UsersListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_users_list, parent, false))

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: UsersListViewHolder, position: Int) {
        val obj = list[position]
        holder.setName(obj.name)
        holder.setLastname(obj.lastname)
        holder.setAge(obj.age)
        holder.setPhone(obj.phone)
        holder.deleteItem(eventDelete)
    }

    class UsersListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun setName(name: String) {
            itemView.item_user_name.text = String.format(GrainChain.getContext().getString(R.string.item_text_name), name)
        }

        fun setLastname(lastname: String) {
            itemView.item_user_lastname.text = String.format(GrainChain.getContext().getString(R.string.item_text_lastname), lastname)
        }

        fun setAge(age: String) {
            itemView.item_user_age.text = String.format(GrainChain.getContext().getString(R.string.item_text_age), age)
        }

        fun setPhone(phone: String) {
            itemView.item_user_phone.text = String.format(GrainChain.getContext().getString(R.string.item_text_phone), phone)
        }

        fun deleteItem(eventDelete: (Int) -> Unit) {
            itemView.setOnLongClickListener {
                eventDelete(adapterPosition)
                true
            }
        }

    }
}