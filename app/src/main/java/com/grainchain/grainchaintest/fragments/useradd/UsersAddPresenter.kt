package com.grainchain.grainchaintest.fragments.useradd

import com.grainchain.grainchaintest.GrainChain
import com.grainchain.grainchaintest.R
import com.grainchain.grainchaintest.database.Contacts
import com.grainchain.grainchaintest.models.ModelSqlite
import com.grainchain.grainchaintest.models.ModelsContract

class UsersAddPresenter(val view: UsersAddContract.View) : UsersAddContract.Presenter {

    private val modelSqlite: ModelsContract.ModelSqlite = ModelSqlite()

    override fun clickSubmit(name: String, lastname: String, age: String, phone: String) {
        when {
            name.isEmpty() -> {
                view.showAlert(GrainChain.getContext().getString(R.string.users_add_error_empty_name))
                return
            }
            lastname.isEmpty() -> {
                view.showAlert(GrainChain.getContext().getString(R.string.users_add_error_empty_lastname))
                return
            }
            age.isEmpty() -> {
                view.showAlert(GrainChain.getContext().getString(R.string.users_add_error_empty_age))
                return
            }
            phone.isEmpty() -> {
                view.showAlert(GrainChain.getContext().getString(R.string.users_add_error_empty_phone))
                return
            }
            phone.length < 10 -> {
                view.showAlert(GrainChain.getContext().getString(R.string.users_add_error_invalid_phone))
                return
            }
        }

        val contact = Contacts(name, lastname, age, phone, "")
        modelSqlite.insertContacts(contact)
        view.notifyList(contact)
        view.clearFields()
    }
}