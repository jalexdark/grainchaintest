package com.grainchain.grainchaintest.fragments.useradd


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.grainchain.grainchaintest.R
import com.grainchain.grainchaintest.closeKeyboard
import com.grainchain.grainchaintest.createAlert
import com.grainchain.grainchaintest.database.Contacts
import com.grainchain.grainchaintest.stringText
import kotlinx.android.synthetic.main.fragment_users_add.*

class UsersAddFragment : Fragment(), UsersAddContract.View, View.OnClickListener {

    companion object {
        fun getNewInstance(): UsersAddFragment = UsersAddFragment()
    }

    private var onContactCreated: UsersAddContract.OnContactCreated? = null
    private lateinit var presenter: UsersAddContract.Presenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_users_add, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = UsersAddPresenter(this)
        initEvents()
    }

    private fun initEvents() {
        user_add_button_submit.setOnClickListener(this)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is UsersAddContract.OnContactCreated) {
            onContactCreated = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnContactCreated")
        }
    }

    override fun onDetach() {
        super.onDetach()
        onContactCreated = null
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.user_add_button_submit -> presenter.clickSubmit(user_add_input_name.stringText, user_add_input_lastname.stringText,
                    user_add_input_age.stringText, user_add_input_phone.stringText)
        }
    }

    override fun notifyList(contact: Contacts) {
        onContactCreated?.onContactCreated(contact)
    }

    override fun showAlert(msg: String) {
        activity!!.createAlert {
            setMessage(msg)
            setPositiveButton(getString(R.string.ok_text)) { dialog, _ -> dialog.dismiss() }
        }
    }

    override fun clearFields() {
        user_add_input_name.stringText = ""
        user_add_input_lastname.stringText = ""
        user_add_input_age.stringText = ""
        user_add_input_phone.stringText = ""
        activity!!.closeKeyboard()
        user_add_input_name.requestFocus()
    }
}
