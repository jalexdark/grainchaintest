package com.grainchain.grainchaintest.models

import android.util.Log
import com.google.gson.JsonObject
import com.grainchain.grainchaintest.api.Api
import com.grainchain.grainchaintest.api.WsMethods
import com.grainchain.grainchaintest.api.responses.AuthResponse
import com.grainchain.grainchaintest.api.responses.BaseResponse
import com.grainchain.grainchaintest.stringToObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.UnknownHostException

class ModelWs : ModelsContract.ModelWs {

    override fun login(user: String, pass: String, event: (Any) -> Unit) {
        val params: HashMap<String, Any> = hashMapOf()
        params["username"] = user
        params["password"] = pass

        val service = Api.getRetrofitInstance(WsMethods::class) as WsMethods
        val call = service.login(params)
        call.enqueue(object : Callback<BaseResponse<AuthResponse>> {
            override fun onFailure(call: Call<BaseResponse<AuthResponse>>, t: Throwable) {
                Log.e("httpResponse", "fail ${t.message}")
                when (t) {
                    is UnknownHostException -> event(600)
                    else -> event(500)
                }
            }

            override fun onResponse(call: Call<BaseResponse<AuthResponse>>, response: Response<BaseResponse<AuthResponse>>) {
                Log.e("httpResponse", "success")
                val body = response.body()
                when {
                    !response.isSuccessful -> event(500)
                    body == null -> event(500)
                    body.errorMessage != null -> {
                        val json = body.errorMessage.stringToObject(JsonObject::class) as JsonObject
                        event(json["body"].asJsonObject["errorMessage"].asString)
                    }
                    body.body != null -> event(body.body.auth)
                    else -> event(500)
                }
            }
        })
    }

}