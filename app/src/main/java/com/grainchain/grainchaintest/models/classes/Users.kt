package com.grainchain.grainchaintest.models.classes

data class Users(
        var name: String = "",
        var lastname: String = "",
        var email: String = "",
        var address: String = ""
)