package com.grainchain.grainchaintest.models

import com.grainchain.grainchaintest.database.Contacts
import com.grainchain.grainchaintest.models.classes.Users

interface ModelsContract {

    interface Model {
        fun setUser(user: Users)
        fun getUser(): Users
        fun clearUser()
        fun setToken(accessToken: String)
        fun getToken(): String
        fun clearToken()
        fun logout()
        fun initContacts()
    }

    interface ModelWs {
        fun login(user: String, pass: String, event: (Any) -> Unit)
    }

    interface ModelSqlite {
        fun getContacts(): List<Contacts>
        fun getContacts(name: String): List<Contacts>
        fun insertContacts(contacts: Contacts)
        fun insertContacts(contacts: List<Contacts>)
        fun deleteContacts()
        fun deleteContact(id: Int)
    }
}