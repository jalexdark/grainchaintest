package com.grainchain.grainchaintest.models

import com.grainchain.grainchaintest.*
import com.grainchain.grainchaintest.database.Contacts
import com.grainchain.grainchaintest.models.classes.Users

class Model : ModelsContract.Model {

    private val TOKEN: String = "TOKEN"
    private val USER: String = "USER"

    private val modelSqlite: ModelsContract.ModelSqlite = ModelSqlite()

    override fun setUser(user: Users) {
        GrainChain.getContext().modifySharedPreferences {
            putString(USER, user.objectToString())
        }
    }

    override fun getUser(): Users {
        val value = GrainChain.getContext().obtainSharedPreference {
            getString(USER, Users().objectToString())!!
        } as String
        return value.stringToObject(Users::class) as Users
    }

    override fun clearUser() {
        GrainChain.getContext().modifySharedPreferences {
            remove(USER)
        }
    }

    override fun setToken(accessToken: String) {
        GrainChain.getContext().modifySharedPreferences {
            putString(TOKEN, accessToken)
        }
    }

    override fun getToken(): String =
            GrainChain.getContext().obtainSharedPreference {
                getString(TOKEN, "")!!
            } as String

    override fun clearToken() {
        GrainChain.getContext().modifySharedPreferences {
            remove(TOKEN)
        }
    }

    override fun logout() {
        this.clearToken()
        this.clearUser()
        modelSqlite.deleteContacts()
    }

    override fun initContacts() {
        val list = listOf(
                Contacts("Jorge", "Torres", "24", "1234567890", ""),
                Contacts("Alejandro", "Solorio", "17", "1234567891", ""),
                Contacts("Josefina", "Cruz", "50", "1234567892", ""),
                Contacts("Ramiro", "Perez", "67", "1234567893", ""),
                Contacts("Guadalupe", "Garcia", "10", "1234567894", ""),
                Contacts("Fernando", "Gonzales", "35", "1234567895", ""),
                Contacts("Francisco", "Aldana", "80", "1234567896", ""),
                Contacts("Orestes", "Gallardo", "28", "1234567897", ""),
                Contacts("Agustin", "Sosa", "21", "1234567898", ""),
                Contacts("Alberto", "Lopez", "18", "1234567899", "")
        )
        modelSqlite.insertContacts(list)
    }
}