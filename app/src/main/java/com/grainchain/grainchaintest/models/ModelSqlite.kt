package com.grainchain.grainchaintest.models

import com.grainchain.grainchaintest.GrainChain
import com.grainchain.grainchaintest.database.AppDataBase
import com.grainchain.grainchaintest.database.Contacts

class ModelSqlite : ModelsContract.ModelSqlite {

    private val database = AppDataBase.getInstance(GrainChain.getContext())

    override fun getContacts(): List<Contacts> {
        val list: List<Contacts>? = database.contacts().getContacts()
        return list ?: listOf()
    }

    override fun insertContacts(contacts: Contacts) {
        database.contacts().insertUser(contacts)
    }

    override fun insertContacts(contacts: List<Contacts>) {
        database.contacts().insertUser(contacts)
    }

    override fun deleteContacts() {
        database.contacts().deleteContacts()
    }

    override fun deleteContact(id: Int) {
        database.contacts().deleteContact(id)
    }

    override fun getContacts(name: String): List<Contacts> {
        return database.contacts().getContacts(name)
    }
}