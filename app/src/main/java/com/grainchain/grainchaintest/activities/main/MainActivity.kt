package com.grainchain.grainchaintest.activities.main

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.grainchain.grainchaintest.R
import com.grainchain.grainchaintest.activities.login.LoginActivity
import com.grainchain.grainchaintest.activities.main.adapter.MainPagerAdapter
import com.grainchain.grainchaintest.closeKeyboard
import com.grainchain.grainchaintest.fragments.useradd.UsersAddContract
import com.grainchain.grainchaintest.fragments.userlist.UsersListFragment
import com.grainchain.grainchaintest.launchActivity
import com.grainchain.grainchaintest.database.Contacts
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar.*

class MainActivity : AppCompatActivity(), MainContract.View, UsersAddContract.OnContactCreated {

    private lateinit var presenter: MainContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter = MainPresenter(this)
        presenter.init(supportFragmentManager)
        setupToolbar()
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.title = presenter.getUserName()
    }

    override fun configurePager(adapter: MainPagerAdapter) {
        main_view_pager.adapter = adapter
        (main_tabs as TabLayout).setupWithViewPager(main_view_pager)
        main_view_pager.addOnPageChangeListener(object: ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p0: Int) {

            }

            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {

            }

            override fun onPageSelected(p0: Int) {
                closeKeyboard()
            }

        })
    }

    override fun onContactCreated(contact: Contacts) {
        val fragment = supportFragmentManager.fragments[0] as UsersListFragment
        fragment.addContact(contact)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.menu_main_item_logout -> {
                presenter.logout()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun goToLogin() {
        launchActivity<LoginActivity> {
            flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        }
    }

    override fun onBackPressed() {
        when {
            main_view_pager.currentItem != 0 -> main_view_pager.currentItem = 0
            else -> super.onBackPressed()
        }
    }
}
