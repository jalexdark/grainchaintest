package com.grainchain.grainchaintest.activities.main

import android.support.v4.app.FragmentManager
import com.grainchain.grainchaintest.activities.main.adapter.MainPagerAdapter
import com.grainchain.grainchaintest.models.Model
import com.grainchain.grainchaintest.models.ModelsContract

class MainPresenter(val view: MainContract.View) : MainContract.Presenter {

    private lateinit var adapter: MainPagerAdapter
    private val model: ModelsContract.Model = Model()

    override fun init(fm: FragmentManager) {
        configureAdapter(fm)
    }

    private fun configureAdapter(fm: FragmentManager) {
        adapter = MainPagerAdapter(fm)
        view.configurePager(adapter)
    }

    override fun getUserName(): String {
        val user = model.getUser()
        return "${user.name} ${user.lastname}"
    }

    override fun logout() {
        model.logout()
        view.goToLogin()
    }
}