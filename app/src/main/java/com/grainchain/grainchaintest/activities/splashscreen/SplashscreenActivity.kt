package com.grainchain.grainchaintest.activities.splashscreen

import android.animation.Animator
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.animation.AccelerateDecelerateInterpolator
import com.grainchain.grainchaintest.R
import com.grainchain.grainchaintest.activities.login.LoginActivity
import com.grainchain.grainchaintest.activities.main.MainActivity
import com.grainchain.grainchaintest.launchActivity
import kotlinx.android.synthetic.main.activity_splashscreen.*

class SplashscreenActivity : AppCompatActivity(), SplashscreenContract.View {

    private lateinit var presenter: SplashscreenContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splashscreen)
        setTheme(R.style.Splashscreen)
        presenter = SplashscreenPresenter(this)
        init()
    }

    private fun init() {
        splashscreen_logo.animate()
                .setDuration(1500L)
                .setInterpolator(AccelerateDecelerateInterpolator())
                .alpha(1F)
                .setListener(object : Animator.AnimatorListener {
                    override fun onAnimationRepeat(p0: Animator?) {

                    }

                    override fun onAnimationCancel(p0: Animator?) {

                    }

                    override fun onAnimationStart(p0: Animator?) {

                    }

                    override fun onAnimationEnd(p0: Animator?) {
                        presenter.checkSession()
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                    }

                })
    }

    override fun goToMain() {
        launchActivity<MainActivity> {
            flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        }
    }

    override fun goToLogin() {
        launchActivity<LoginActivity> {
            flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        }
    }
}
