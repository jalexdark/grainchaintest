package com.grainchain.grainchaintest.activities.splashscreen

import com.grainchain.grainchaintest.models.Model
import com.grainchain.grainchaintest.models.ModelsContract

class SplashscreenPresenter(val view: SplashscreenContract.View) : SplashscreenContract.Presenter {

    private val model: ModelsContract.Model = Model()

    override fun checkSession() {
        if (model.getToken().isEmpty()) {
            view.goToLogin()
        } else {
            view.goToMain()
        }
    }
}