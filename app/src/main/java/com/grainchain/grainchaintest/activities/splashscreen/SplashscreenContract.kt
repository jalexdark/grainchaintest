package com.grainchain.grainchaintest.activities.splashscreen

interface SplashscreenContract {
    interface View {
        fun goToMain()
        fun goToLogin()
    }

    interface Presenter {
        fun checkSession()
    }
}