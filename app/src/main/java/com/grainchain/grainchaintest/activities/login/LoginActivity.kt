package com.grainchain.grainchaintest.activities.login

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.grainchain.grainchaintest.*
import com.grainchain.grainchaintest.activities.main.MainActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), LoginContract.View, View.OnClickListener {

    private lateinit var progress: AlertDialog
    private lateinit var presenter: LoginContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initEvents()
        presenter = LoginPresenter(this)
        presenter.checkDebug()
        progress = createProgress {
            setView(layoutInflater.inflate(R.layout.progress_dialog, null, false))
        }
    }

    private fun initEvents() {
        login_input_button.setOnClickListener(this)
    }

    override fun initView(user: String, pass: String) {
        login_input_user.stringText = user
        login_input_pass.stringText = pass
    }

    override fun goToMain() {
        launchActivity<MainActivity> {
            flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        }
    }

    override fun showProgress() {
        progress.show()
    }

    override fun hideProgress() {
        progress.hide()
    }

    override fun showAlert(msg: String) {
        createAlert {
            setMessage(msg)
            setPositiveButton(getString(R.string.ok_text)) { dialog, _ -> dialog.dismiss() }
        }
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.login_input_button -> presenter.login(login_input_user.stringText, login_input_pass.stringText)
        }
    }
}
