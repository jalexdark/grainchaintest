package com.grainchain.grainchaintest.activities.login

interface LoginContract {

    interface View {
        fun initView(user: String, pass: String)
        fun goToMain()
        fun showProgress()
        fun hideProgress()
        fun showAlert(msg: String)
    }

    interface Presenter {
        fun checkDebug()
        fun login(user: String, pass: String)
    }
}