package com.grainchain.grainchaintest.activities.main.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.grainchain.grainchaintest.GrainChain
import com.grainchain.grainchaintest.R
import com.grainchain.grainchaintest.fragments.useradd.UsersAddFragment
import com.grainchain.grainchaintest.fragments.userlist.UsersListFragment

class MainPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> UsersListFragment.getNewInstance()
            1 -> UsersAddFragment.getNewInstance()
            else -> UsersListFragment.getNewInstance()
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> GrainChain.getContext().getString(R.string.main_tabs_title_user_list)
            1 -> GrainChain.getContext().getString(R.string.main_tabs_title_user_add)
            else -> super.getPageTitle(position)
        }
    }

    override fun getCount(): Int = 2

}