package com.grainchain.grainchaintest.activities.login

import com.grainchain.grainchaintest.BuildConfig
import com.grainchain.grainchaintest.GrainChain
import com.grainchain.grainchaintest.R
import com.grainchain.grainchaintest.api.responses.Auth
import com.grainchain.grainchaintest.models.Model
import com.grainchain.grainchaintest.models.ModelWs
import com.grainchain.grainchaintest.models.ModelsContract

class LoginPresenter(val view: LoginContract.View) : LoginContract.Presenter {

    val modelWs: ModelsContract.ModelWs = ModelWs()
    val model: ModelsContract.Model = Model()

    override fun checkDebug() {
        if (BuildConfig.DEBUG) {
            view.initView(BuildConfig.USER_DEVELOP, BuildConfig.PASS_DEVELOP)
        }
    }

    override fun login(user: String, pass: String) {
        if (user.isEmpty()) {
            view.showAlert(GrainChain.getContext().getString(R.string.login_error_empty_user))
            return
        }
        if (pass.isEmpty()) {
            view.showAlert(GrainChain.getContext().getString(R.string.login_error_empty_pass))
            return
        }
        view.showProgress()
        modelWs.login(user, pass) {
            view.hideProgress()
            when (it) {
                is Int -> {
                    when (it) {
                        500 -> view.showAlert(GrainChain.getContext().getString(R.string.error_default))
                        600 -> view.showAlert(GrainChain.getContext().getString(R.string.error_without_network))
                        else -> view.showAlert(GrainChain.getContext().getString(R.string.error_default))
                    }
                }
                is String -> view.showAlert(it)
                is Auth -> {
                    model.initContacts()
                    model.setUser(it.user)
                    model.setToken(it.access_token)
                    view.goToMain()
                }
                else -> view.showAlert(GrainChain.getContext().getString(R.string.error_default))
            }
        }
    }
}