package com.grainchain.grainchaintest.activities.main

import android.support.v4.app.FragmentManager
import com.grainchain.grainchaintest.activities.main.adapter.MainPagerAdapter

interface MainContract {
    interface View {
        fun configurePager(adapter: MainPagerAdapter)
        fun goToLogin()
    }

    interface Presenter {
        fun init(fm: FragmentManager)
        fun getUserName(): String
        fun logout()
    }
}