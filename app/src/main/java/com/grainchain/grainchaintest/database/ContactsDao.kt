package com.grainchain.grainchaintest.database

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query

@Dao
interface ContactsDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertUser(contact: Contacts)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertUser(list: List<Contacts>)

    @Query("SELECT * FROM CONTACTS")
    fun getContacts(): List<Contacts>

    @Query("SELECT * FROM CONTACTS WHERE name LIKE '%' || :name || '%'")
    fun getContacts(name: String): List<Contacts>

    @Query("DELETE FROM CONTACTS")
    fun deleteContacts()

    @Query("DELETE FROM CONTACTS WHERE id = :id")
    fun deleteContact(id: Int)
}