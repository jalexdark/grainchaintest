package com.grainchain.grainchaintest.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "CONTACTS")
data class Contacts(
        @ColumnInfo(name = "name")
        var name: String,
        @ColumnInfo(name = "lastname")
        var lastname: String,
        @ColumnInfo(name = "age")
        var age: String,
        @ColumnInfo(name = "phone")
        var phone: String,
        @ColumnInfo(name = "image")
        var image: String,
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "id")
        var id: Int? = null
) {
    @Ignore
    private constructor() : this("", "", "", "", "", 0)
}
