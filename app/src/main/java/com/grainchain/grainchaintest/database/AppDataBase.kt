package com.grainchain.grainchaintest.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context

@Database(entities = [Contacts::class], version = 1)
abstract class AppDataBase : RoomDatabase() {
    companion object {
        private var db: AppDataBase? = null

        fun getInstance(context: Context): AppDataBase {


            if (db == null) {
                db = Room
                        .databaseBuilder(context, AppDataBase::class.java, "grainchain.database")
                        .allowMainThreadQueries()
                        .build()
            }


            return db!!
        }
    }

    abstract fun contacts(): ContactsDao
}