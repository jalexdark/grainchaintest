package com.grainchain.grainchaintest.api.responses

import com.grainchain.grainchaintest.models.classes.Users

data class AuthResponse(
        val status: String,
        val auth: Auth
)

data class Auth(
        val user: Users,
        val access_token: String
)