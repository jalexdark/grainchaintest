package com.grainchain.grainchaintest.api

import com.grainchain.grainchaintest.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import kotlin.reflect.KClass

class Api {
    companion object {
        private lateinit var retrofitInstance: Retrofit

        fun getRetrofit(): Retrofit = retrofitInstance

        fun getRetrofitInstance(classType: KClass<*>): Any {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY

            val client = OkHttpClient.Builder()
                    .addInterceptor(interceptor)
                    .build()

            retrofitInstance = Retrofit.Builder()
                    .baseUrl(BuildConfig.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build()

            return retrofitInstance.create(classType.java)
        }
    }
}