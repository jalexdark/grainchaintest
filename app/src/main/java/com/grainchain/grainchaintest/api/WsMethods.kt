package com.grainchain.grainchaintest.api

import com.grainchain.grainchaintest.api.responses.AuthResponse
import com.grainchain.grainchaintest.api.responses.BaseResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface WsMethods {
    @POST("test")
    fun login(@Body params: HashMap<String, Any>): Call<BaseResponse<AuthResponse>>
}