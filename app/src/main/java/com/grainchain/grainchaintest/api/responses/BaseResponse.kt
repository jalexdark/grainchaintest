package com.grainchain.grainchaintest.api.responses

data class BaseResponse<T>(
        val statusCode: Int,
        val body: T?,
        val errorMessage: String?
)