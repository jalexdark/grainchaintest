package com.grainchain.grainchaintest

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import com.google.gson.Gson
import kotlin.reflect.KClass

private fun pathSharedPreferences(): String = "com.grainchain.grainchaintest"

fun Context.modifySharedPreferences(block: SharedPreferences.Editor.() -> Unit) {
    val sharedPreferences = this.getSharedPreferences(pathSharedPreferences(), Context.MODE_PRIVATE)
    val editor = sharedPreferences.edit()
    editor.block()
    editor.apply()
}

fun Context.obtainSharedPreference(block: SharedPreferences.() -> Any): Any {
    val sharedPreferences = this.getSharedPreferences(pathSharedPreferences(), Context.MODE_PRIVATE)
    return sharedPreferences.block()
}

fun Any.objectToString(): String =
        Gson().toJson(this)

fun String.stringToObject(type: KClass<*>): Any =
        Gson().fromJson(this, type.java)

inline fun <reified T> Context.launchActivity(block: Intent.() -> Unit) {
    val intent = Intent(this, T::class.java)
    intent.block()
    startActivity(intent)
}

var EditText.stringText: String
    get() = text.toString()
    set(value) = setText(value)

fun Context.createAlert(block: AlertDialog.Builder.() -> Unit) {
    val alert = AlertDialog.Builder(this)
    alert.block()
    alert.show()
}

fun Context.createProgress(block: AlertDialog.Builder.() -> Unit): AlertDialog {
    val alert = AlertDialog.Builder(this)
    alert.block()
    alert.setCancelable(false)
    val dialog = alert.create()
    dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    return dialog
}

fun Activity.closeKeyboard() {
    val input = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    input.hideSoftInputFromWindow(window.decorView.rootView.windowToken, 0)
}