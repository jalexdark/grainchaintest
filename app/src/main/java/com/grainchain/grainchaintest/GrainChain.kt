package com.grainchain.grainchaintest

import android.app.Application
import android.content.Context

class GrainChain : Application() {

    companion object {
        private var appContext: Context? = null
        fun getContext(): Context = appContext!!
    }

    override fun onCreate() {
        super.onCreate()
        appContext = this
    }

}